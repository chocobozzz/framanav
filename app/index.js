import Vue from 'vue';
import VueRouter from 'vue-router';
import VueI18n from 'vue-i18n';
import PortalVue from 'portal-vue';
import {
  LayoutPlugin,
  ModalPlugin,
  ButtonPlugin,
  PopoverPlugin,
  CardPlugin,
  FormPlugin,
  FormGroupPlugin,
  InputGroupPlugin,
  FormInputPlugin,
  FormSelectPlugin,
  FormTextareaPlugin,
  FormFilePlugin,
  SpinnerPlugin,
  BadgePlugin,
  ListGroupPlugin,
  AlertPlugin,
  CollapsePlugin,
  SidebarPlugin,
  CarouselPlugin,
  ButtonGroupPlugin,
} from 'bootstrap-vue';

import is from 'vue-fs-commons/app/plugins/is';
import cookie from 'vue-fs-commons/app/plugins/cookie';
import merge from 'vue-fs-commons/app/plugins/merge';
import text from 'vue-fs-commons/app/plugins/text';
import SVGIcon from 'vue-fs-commons/app/components/commons/SVGIcon.vue';
import App from './App.vue';
import globalStorage from './plugins/globalstorage';

import './assets/scss/main.scss';

Vue.use(LayoutPlugin);
Vue.use(ModalPlugin);
Vue.use(ButtonPlugin);
Vue.use(ButtonGroupPlugin);
Vue.use(PopoverPlugin);
Vue.use(CardPlugin);
Vue.use(FormPlugin);
Vue.use(FormInputPlugin);
Vue.use(FormGroupPlugin);
Vue.use(InputGroupPlugin);
Vue.use(FormSelectPlugin);
Vue.use(FormTextareaPlugin);
Vue.use(FormFilePlugin);
Vue.use(SpinnerPlugin);
Vue.use(BadgePlugin);
Vue.use(ListGroupPlugin);
Vue.use(AlertPlugin);
Vue.use(CollapsePlugin);
Vue.use(SidebarPlugin);
Vue.use(CarouselPlugin);

Vue.use(VueRouter);
Vue.use(VueI18n);
Vue.use(PortalVue);

Vue.use(is);
Vue.use(cookie);
Vue.use(merge);
Vue.use(globalStorage);
Vue.use(text);
Vue.component('svg-icon', SVGIcon);

const defaultLocale = 'fr';
const locales = {};
const pages = [];
let commons = [];

// Import locales list
// in locales/[lg]/[file].yml
let req = require.context('./locales/', true, /\.\/.*\/.*\.yml$/);
req.keys().forEach((key) => {
  const lg = key.split('/')[1];
  const file = key.split('/')[2].replace(/\.yml/, '');
  if (locales[lg] === undefined) {
    locales[lg] = [];
  }
  if (!locales[lg].includes(file)) {
    locales[lg].push(file);
  }
});

// Import pages list
req = require.context('./components/pages', false, /\.vue$/);
req.keys().forEach((key) => {
  pages.push(key.replace(/\.\/(.*)\.vue/, '$1'));
});

// Import commons data list
req = require.context('../node_modules/vue-fs-commons/app/data/commons/', false, /\.yml$/);
req.keys().forEach((key) => {
  commons.push(key.replace(/\.\/(.*)\.yml/, '$1'));
});

let lang = '';
const html = document.getElementsByTagName('html');
const meta = document.getElementsByTagName('script');

if (html[0].getAttribute('xml:lang')) {
  lang = html[0].getAttribute('xml:lang');
} else if (html[0].getAttribute('lang')) {
  lang = html[0].getAttribute('lang');
} else if (html[0].getAttribute('locale')) {
  lang = html[0].getAttribute('locale');
} else {
  for (let i = 0; i < meta.length; i += 1) {
    if ((meta[i].getAttribute('http-equiv') && meta[i].getAttribute('content')
      && meta[i].getAttribute('http-equiv').indexOf('Language') > -1)
      || (meta[i].getAttribute('property') && meta[i].getAttribute('content')
          && meta[i].getAttribute('property').indexOf('locale') > -1)) {
      lang = meta[i].getAttribute('content');
    }
  }
}

const userLang = navigator.languages
  || [navigator.language || navigator.userLanguage];
let defaultRouteLang = '';

const messages = {};
const numberFormats = {};
messages.locales = require('../node_modules/vue-fs-commons/app/locales/lang.yml'); // eslint-disable-line
messages.locales.available = Object
  .keys(messages.locales)
  .filter(n => Object.keys(locales).includes(n)
    && (locales[n].includes('main') || locales[n].includes('_main')));
messages.locales.visible = Object
  .keys(messages.locales)
  .filter(n => Object.keys(locales).includes(n) && (locales[n].includes('_main')));

// Data import
let data = {};
let project = {};
const scripts = document.getElementsByTagName('script');
project = require('./data/project.yml') || {}; // eslint-disable-line
if (Array.isArray(project.commons) && project.commons.length > 0) {
  [commons] = [project.commons];
}
for (let i = 0; i < commons.length; i += 1) {
  req = require(`../node_modules/vue-fs-commons/app/data/commons/${commons[i]}.yml`) || {}; // eslint-disable-line
  data[commons[i]] = merge.$(data[commons[i]], JSON.parse(JSON.stringify(req)));
}
data = merge.$(data, JSON.parse(JSON.stringify(project)));

Object.assign(data, {
  host: window.location.host,
  url: window.location.href,
  '/': `/${process.env.BASE_URL.replace(/(.+)/, '$1/')}`,
  inframe: window.top.location !== window.self.document.location ? 'true' : 'false',
  hash: window.location.hash.replace('#', ''),
  date: process.env.DATE,
  year: {
    current: (new Date().getFullYear()).toString(),
    next: (new Date().getFullYear() + 1).toString(),
  },
});
if (/\/nav.js$/.test(scripts[scripts.length - 1].src)) {
  data.self = new URL(scripts[scripts.length - 1].src, data.url).href;
} else {
  const findNav = /nav.src = '(.*)';\n/g.exec(scripts[scripts.length - 1].innerText);
  data.self = (findNav !== null && findNav[1] !== undefined)
    ? new URL(findNav[1], data.url).href
    : 'https://framasoft.org/nav/nav.js';
}
if (process.env.NODE_ENV === 'production'
  && data.meta.canonical !== undefined
  && /^http/.test(data.meta.canonical)) {
  data.baseurl = data.meta.canonical.replace(/(.+?)\/?$/, '$1/');
} else {
  data.baseurl = `${data.self.split('/').slice(0, -1).join('/')}/`;
}

/** Only for nav */
data.site = data.host.replace(/^(www|test)\./i, '').replace(/\.(com|net|org|fr|pro)$/i, '');
data.name = data.site[0].toUpperCase() + data.site.slice(1).replace('.framasoft', ''); // Nom du service
data.lname = data.name.toLowerCase();

// [site] Cleaning (shortest name for config)
data.site = data.site.replace(/framand/i, 'and')
  .replace(/framage/i, 'age')
  .replace(/framae/i, 'mae')
  .replace(/(\.framasoft|frama\.)/i, '')
  .replace(/framin/i, 'min')
  .replace(/frame/i, 'me')
  .replace(/frama/i, '')
  .replace(/localhost:8080/i, 'listes');

// [site] Aliases
if (/huit.re/.test(data.host)) { data.site = 'link'; }

// [site] Subdomains
if (/framaboard/.test(data.host)) { data.site = 'board'; }
if (/framacalc/.test(data.host)) { data.site = 'calc'; }
if (/framadate/.test(data.host)) { data.site = 'date'; }
if (/framadrop/.test(data.host)) { data.site = 'drop'; }

// [site] Exceptions Framapad
if (/(mypads2?|beta3).framapad.org/.test(data.host)) {
  data.site = 'mypads';
  data.name = 'Framapad';
}
if ((/.framapad/.test(data.host) && !/mypads./.test(data.host))
  || (/(mypads2?|beta3).framapad/.test(data.host) && /\/p\//.test(data.url))) {
  data.site = 'etherpad';
  data.name = 'Framapad';
}

// [site] Exception Framawiki
if (/wiki.framasoft.org/.test(data.host)) {
  data.site = 'wikifs';
}
/** </> */

data.txt = data.txt || {};
data.html = data.html || {};

const routes = [];
let msg = {};
messages.locales.map = {};

// Import locales
messages.locales.available.forEach((k) => {
  /* eslint-disable */
  messages[k] = {};
  messages[k].lang = k;

  numberFormats[k] = {};
  numberFormats[k].eur = {
    style: 'currency',
    currency: 'EUR',
    maximumFractionDigits: 0,
    minimumFractionDigits: 0,
  };

  // Import data
  msg = {txt: {}, html: {}, tmp: {}};
  msg = merge.$(msg, JSON.parse(JSON.stringify(data)));

  const mainFile = locales[k].filter(filename => /^_?main$/.test(filename));
  if (mainFile.length > 0) {
    // Init with ../node_modules/vue-fs-commons/app/locales/lg/_commons.yml
    req = require(`../node_modules/vue-fs-commons/app/locales/${k}/_commons.yml`) || {};
    msg.tmp = merge.$(msg.tmp, JSON.parse(JSON.stringify(req)));
    // then import locales/lg/_main.yml (active and visible)
    // or locales/lg/main.yml (active and hidden)
    req = require(`./locales/${k}/${mainFile[0]}.yml`) || {};
    msg.tmp = merge.$(msg.tmp, JSON.parse(JSON.stringify(req)));
  }

  // locales/lg/*.yml
  for (let i = 0; i < locales[k].length; i += 1) {
    const file = locales[k][i];
    if (!/^_?main$/.test(file)) {
      msg.tmp[file] = msg.tmp[file] || {};
      req = require(`./locales/${k}/${file}.yml`) || {};
      msg.tmp[file] = merge.$(msg.tmp[file], JSON.parse(JSON.stringify(req)));
    }
  }

  msg = merge.$(msg, msg.tmp);

  // Create locale template to be able to use the English text
  // instead of the yaml key for translation
  if (k === 'en') {
    Object.assign(messages.locales.map,
      ...function _flatten(o, k) {
        return [].concat(...Object.keys(o)
          .map(i => {
            const tmp = k ? `${k}.${i}` : i;
            return typeof o[i] !== 'object'
              ? ({[JSON.stringify(o[i].trim().replace(/\n[ ]+/g, '\n'))]: `@:${tmp}`})
              : _flatten(o[i], tmp);
            }
          )
        );
      }(msg.tmp)
    );
  }
  delete msg.tmp;

  Object.keys(msg.color).forEach((j) => {
    if (msg.txt[j] === undefined) {
      msg.txt[j] = text.textTransform(msg.color[j], '-t');
    }
  });
  Object.keys(msg.link).forEach((j) => {
    if (msg.html[j] === undefined) {
      if (msg.color[j] !== undefined) {
        msg.html[j] = `<a href="@:link.${j}">@:color.${j}</a>`;
      } else if (msg.txt[j] !== undefined) {
        msg.html[j] = `<a href="@:link.${j}">@:txt.${j}</a>`;
      }
    }
  });
  messages[k] = merge.$(messages[k], msg);
  /* eslint-enable */

  // Localized routes
  for (let j = 0; j < pages.length; j += 1) {
    const component = require(`./components/pages/${pages[j]}.vue`); // eslint-disable-line
    routes.push({
      path: `/${k}${pages[j].toLowerCase().replace(/^/, '/').replace('/home', '')}`,
      component: component.default,
      meta: { id: pages[j].toLowerCase(), lang: k },
    });
  }
});

// define defaultRouteLang
for (let j = 0; j < userLang.length; j += 1) { // check if user locales
  const lg = userLang[j].substring(0, 2).toLowerCase();
  if (defaultRouteLang === '' && Object.keys(locales).includes(lg)) { // matches with app locales
    defaultRouteLang = lg;
  }
}

/* Check if lang is avalaible */
if (!messages.locales.available.includes(lang)) {
  if (messages.locales.available.includes(lang.substr(0, 2))) {
    /* lang === (fr_FR|en_GB|…) */
    lang = lang.substr(0, 2);
  } else {
    /* lang === (it|sv|ø|…) */
    lang = defaultLocale;
  }
} /*   lang === (en|fr) */

// Create VueI18n instance with options
const i18n = new VueI18n({
  locale: lang,
  fallbackLocale: defaultLocale,
  messages,
  numberFormats,
  silentTranslationWarn: true,
});

// Override $t() for markdown and formatting
Vue.prototype.$t = (key, locale, values) => text.$t(key, locale, values, i18n);

Vue.prototype.toggleFramaSidebar = () => {
  const visible = (document.getElementsByTagName('html')[0]
    .getAttribute('data-sidebar') || false) === 'true';
  document.getElementsByTagName('html')[0].setAttribute('data-sidebar', !visible);
};

const loadNav = () => {
  if (document.getElementById('f-nav') === null) {
    document.querySelector('body')
      .insertAdjacentHTML('afterbegin', '<div id="f-nav"></div>');
  }
  new Vue({ // eslint-disable-line no-new
    el: '#f-nav',
    i18n,
    mounted() {
      // You'll need this for renderAfterDocumentEvent.
      document.dispatchEvent(new Event('render-event'));
    },
    render: h => h(App),
  });
};

if (!/\/nav.js$/.test(scripts[scripts.length - 1].src)) {
  loadNav();
} else {
  window.addEventListener('DOMContentLoaded', () => { loadNav(); });
}
